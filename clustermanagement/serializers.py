
from .models import Subnet, Region, VPC, InstanceProvider, InstanceSize, NodeProfile, TMVersion, Cluster, Node
from rest_framework import serializers


class InstanceProviderSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = InstanceProvider
		fields = ('id', "name", "description")

class SubnetSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Subnet
		fields = ('id', "vanity_name", "cidr", "vpc")

class VPCSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = VPC
		fields = ('id', "region", "name")


class RegionSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Region
		fields = ('id', "provider", "name")

class InstanceSizeSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = InstanceSize
		fields = ('id', 'name', 'cpu_count', 'ram_amount')


class NodeProfileSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = NodeProfile
		fields = ('id', 'name',)


class TMVersionSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = TMVersion
		fields = ('id', 'name',)


class NodeSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Node
		fields = ('id', 'name', 'cluster', 'creator', 'creation_date', 'description', 'tm_version', 'services')


class ClusterSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = Cluster
		fields = ('id', 'name', 'cluster_uuid', 'creator', 'creation_date', 'description', 'tm_version', 'region')


class ClusterCreationNodeSerializer(serializers.Serializer):
	profile_id = serializers.IntegerField()
	instance_size_id = serializers.IntegerField()


class ClusterCreationSerializer(serializers.Serializer):

	name = serializers.CharField()
	tm_version_id = serializers.IntegerField()
	subnet_id = serializers.IntegerField()
	nodes = ClusterCreationNodeSerializer(many=True)


