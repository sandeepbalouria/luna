from django.contrib import admin

# Register your models here.

from .models import ClusterTemplate, Cluster, Node, Tenant, TenantCluster, Service, ServiceState, ServiceNode,\
	ClusterLease, ClusterJob, ClusterActionType, ClusterAction, ServiceAttribute, ServiceNodeAttribute, NodeAttribute ,\
	NodeProfile, InstanceProvider, InstanceSize, TMVersion, Region, VPC, Subnet, ServiceProfile


admin.site.register(ClusterTemplate)
admin.site.register(Cluster)
admin.site.register(Node)
admin.site.register(Tenant)
admin.site.register(TenantCluster)
admin.site.register(Service)
admin.site.register(ServiceState)
admin.site.register(ServiceNode)
admin.site.register(ClusterLease)
admin.site.register(ClusterJob)
admin.site.register(ClusterActionType)
admin.site.register(ClusterAction)
admin.site.register(ServiceAttribute)
admin.site.register(ServiceNodeAttribute)
admin.site.register(NodeAttribute)
admin.site.register(NodeProfile)
admin.site.register(InstanceProvider)
admin.site.register(InstanceSize)
admin.site.register(TMVersion)
admin.site.register(Region)
admin.site.register(VPC)
admin.site.register(Subnet)
admin.site.register(ServiceProfile)