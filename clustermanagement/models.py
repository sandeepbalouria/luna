from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone
from django.contrib.auth.models import User
# Create your models here.


class ClusterTemplate(models.Model):
	name = models.CharField(max_length=200, blank=False, null=False)
	creator = models.ForeignKey(User, null=False)
	description = models.TextField(max_length=500)
	creation_date = models.DateTimeField(default=timezone.now)

	def __unicode__(self):
		return "%s" % (self.name,)

	class Meta:
		ordering = ["-creation_date"]


class Region(models.Model):
	provider = models.ForeignKey('InstanceProvider')
	name = models.CharField(max_length=50)

	def __unicode__(self):
		return "%s (%s)"%(self.name, unicode(self.provider))


class Subnet(models.Model):
	vanity_name = models.CharField(max_length=200)
	name = models.CharField(max_length=50)
	cidr = models.CharField(max_length=20)
	vpc = models.ForeignKey('VPC')

	def __unicode__(self):
		return "%s (%s, %s)"%(self.name, self.vanity_name, self.cidr)


class VPC(models.Model):
	name = models.CharField(max_length=15)
	region = models.ForeignKey(Region, null=True)

	def __unicode__(self):
		return "%s (%s)" % (self.name, unicode(self.region))

class Cluster(models.Model):
	name = models.CharField(max_length=200, blank=False, null=False, unique=False)
	cluster_uuid = models.UUIDField(null=True, blank=True)
	creator = models.ForeignKey(User, null=False)
	creation_date = models.DateTimeField(default=timezone.now)
	description = models.TextField(max_length=500)
	region = models.ForeignKey('Region', null=False)
	tm_version = models.ForeignKey('TMVersion', null=True)
	cluster_template = models.ForeignKey(ClusterTemplate, null=True, blank=True)

	def __unicode__(self):
		return "%s (%s)" % (self.name, self.region)

	class Meta:
		ordering = ("name",)


	def get_salt_mapfile_string(self):
		result = ""
		#todo: create the salt YAML map file using all our information
		for node in self.node_set.all():
			result += "\n\n"
			result += node.get_salt_mapfile_string()

		with open('the_map_file.yaml',mode='w') as f:
			f.write(result)

		return result

	def get_glu_model_string(self):
		result = ""
		# todo: create the glu json file using all our information
		return result


class Node(models.Model):
	name = models.CharField(max_length=200, blank=False, null=False)
	cluster = models.ForeignKey(Cluster, null=False)
	creator = models.ForeignKey(User, null=False)
	creation_date = models.DateTimeField(default=timezone.now)
	description = models.TextField(max_length=500)
	tm_version = models.ForeignKey('TMVersion', null=True)
	services = models.ManyToManyField('Service', through='ServiceNode')
	instance_size = models.ForeignKey('InstanceSize')
	subnet = models.ForeignKey(Subnet, null=True)
	provisioning_status = models.PositiveSmallIntegerField(default=0)
	node_profile = models.ForeignKey('NodeProfile', null=True)


	def __unicode__(self):
		return "%s (%s)" % (self.name, self.cluster.name)

	class Meta:
		ordering = ('name',)

	def get_salt_mapfile_string(self):
		result = ""
		# todo: create the salt YAML map file using all our information
		context = {'node':self}
		result = render_to_string(self.node_profile.map_template_name, context)
		return result

class TMVersion(models.Model):
	name = models.CharField(max_length=200, blank=False, null=False)

	def __unicode__(self):
		return "%s"%self.name

	class Meta:
		ordering = ("name",)

class InstanceProvider(models.Model):
	name = models.CharField(max_length=20)
	description = models.TextField()

	def __unicode__(self):
		return "%s" % (self.name)

	class Meta:
		ordering = ("name",)


class InstanceSize(models.Model):
	provider = models.ForeignKey(InstanceProvider)
	name = models.CharField(max_length='30')
	description = models.TextField(blank=True)
	cpu_count = models.PositiveSmallIntegerField(blank=False)
	ram_amount = models.BigIntegerField(blank=False, verbose_name="RAM (bytes)")

	class Meta:
		ordering = ("name",)

	def __unicode__(self):
		return "%s (%i cores/%0.02fGB)" % (self.name, self.cpu_count, self.ram_amount/1073741824.0)


class NodeAttribute(models.Model):
	node = models.ForeignKey(Node)
	key = models.CharField(max_length=100)
	value = models.TextField()

	class Meta:
		ordering = ('node', 'key')


class Tenant(models.Model):
	name = models.CharField(max_length=200, blank=False, null=False)
	description = models.TextField(max_length=500)
	clusters = models.ManyToManyField(Cluster, through='TenantCluster')

	def __unicode__(self):
		return "%s (%s)" % (self.name, ",".join([c.cluster.name for c in self.clusters.all()]))


class TenantCluster(models.Model):
	cluster = models.ForeignKey(Cluster)
	tenant = models.ForeignKey(Tenant)
	mover = models.ForeignKey(User)
	move_date = models.DateTimeField(default=timezone.now)
	notes = models.TextField(max_length="500", blank=True)

	def __unicode__(self):
		return "%s@%s" % (self.tenant.name, self.cluster.name)


class Service(models.Model):
	name = models.CharField(max_length=200, blank=False, null=False)
	description = models.TextField(max_length=500, blank=True, null=False)

	def __unicode__(self):
		return "%s" % (self.name,)

	class Meta:
		ordering = ("name",)

class ServiceState(models.Model):
	name = models.CharField(max_length=50)

	def __unicode__(self):
		return "%s" % (self.name,)

	class Meta:
		ordering = ("name",)

class ServiceNode(models.Model):
	service = models.ForeignKey(Service)
	node = models.ForeignKey(Node)
	service_index = models.PositiveSmallIntegerField(default=1)
	activation_date = models.DateTimeField(default=timezone.now)
	desired_state = models.ForeignKey(ServiceState)
	tm_version = models.ForeignKey('TMVersion', null=True)

	def __unicode__(self):
		return "%s@%s (%s)" % (self.service.name, self.node.name, self.desired_state.name)

class ServiceAttribute(models.Model):
	#Stores default attributes for Services
	service_node = models.ForeignKey(Service)
	key = models.CharField(max_length=100)
	value = models.TextField()

class ServiceNodeAttribute(models.Model):
	# Stores attributes for services running on a particular node.
	service_node = models.ForeignKey(ServiceNode)
	key = models.CharField(max_length=100)
	value = models.TextField()

class ClusterLease(models.Model):
	cluster = models.ForeignKey(Cluster)
	user = models.ForeignKey(User)
	start_date = models.DateTimeField(default=timezone.now)
	end_date = models.DateTimeField(default=timezone.now)
	description = models.TextField(max_length=500, blank=True, null=False)


class ClusterJob(models.Model):
	cluster = models.ForeignKey(Cluster)
	salt_jid = models.CharField(max_length=75, blank=False, null=False)
	description = models.TextField(max_length=500, blank=True, null=False)
	creator = models.ForeignKey(User)
	creation_date = models.DateTimeField(default=timezone.now)

	def __unicode__(self):
		return "%s@%s" % (self.salt_jid, self.cluster.name,)


class ClusterActionType(models.Model):
	name = models.CharField(max_length=200)

	def __unicode__(self):
		return "%s" % (self.name,)

	class Meta:
		ordering = ("name",)


class ClusterAction(models.Model):
	cluster_job = models.ForeignKey(ClusterJob)
	salt_jid = models.CharField(max_length=75, blank=False, null=False)
	description = models.TextField(max_length=500, blank=True, null=False)
	action_type = models.ForeignKey(ClusterActionType)
	target = models.CharField(max_length=200, blank=False, null=False, default="")

	def __unicode__(self):
		return "%s@%s"

class ServiceProfile(models.Model):
	service = models.ForeignKey(Service)

class NodeProfile(models.Model):
	'''
	A node profile is a collection of preset values used to define a node.
	For now, it is simply a template file that gets filled with information based on the cluster and node that it will
	spawn
	'''
	name = models.CharField(max_length=50)
	services = models.ManyToManyField(Service)
	#the map template contains a template string that will be filled by the django template system to create a map file.
	#the context for the template will be created by the information contained in the node.
	map_template_name = models.CharField(max_length=200, blank=True, null=True)
	salt_instance_profile_name = models.CharField(max_length=200, blank=True, null=True)

	def __unicode__(self):
		return "%s"%(self.name)

	class Meta:
		ordering = ('name',)

