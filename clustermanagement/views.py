import random
import uuid
from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.metadata import SimpleMetadata

from .serializers import InstanceSizeSerializer, NodeProfileSerializer, TMVersionSerializer, ClusterCreationSerializer, \
	SubnetSerializer, RegionSerializer, InstanceProviderSerializer, VPCSerializer, ClusterSerializer
from .models import Subnet, InstanceSize, NodeProfile, TMVersion, Cluster, Node, Region, InstanceProvider, VPC, \
	ServiceNode, \
	ServiceState, ClusterJob


class ClusterCreationPageView(TemplateView):
	template_name = "clustermanagement/cluster_creation.html"

	def get_context_data(self, **kwargs):
		context = {}
		return context

class ClusterListPageView(TemplateView):
	template_name = "clustermanagement/cluster_list.html"

	def get_context_data(self, **kwargs):
		context = {}
		return context

# ######### API views ###########

class ClusterCreationView(APIView):
	metadata_class = SimpleMetadata
	description = '''Will create a new cluster and all it's nodes.'''

	def get_view_description(self, html=False):
		return self.description

	def get_serializer(self):
		"""
		Overriding this method and returning a ClusterCreationSerializer allows the SimpleMetadata metadata class to
		get insight into the serializer that our post will use. This is used to display the serializer fields when an
		OPTIONS request is made to the view.
		"""
		return ClusterCreationSerializer()

	def post(self, request):

		serializer = ClusterCreationSerializer(data=request.data)

		if serializer.is_valid():
			# TODO: Use the serializer data to create the Django objects that make up this cluster. That means creating

			### PARSE THE INPUT AND MAKE SURE ALL PARAMETERS ARE VALID, PARTICULARLY THE IDS THAT MATCH A MODEL ###
			# todo : maybe check permissions for some of the parameters, ie. are we allowed to create clusters in subnet x?
			cluster_name = serializer.validated_data['name']
			tm_version = get_object_or_404(TMVersion, pk=serializer.validated_data["tm_version_id"])
			subnet = get_object_or_404(Subnet, pk=serializer.validated_data["subnet_id"])
			requested_nodes = []
			running_state = get_object_or_404(ServiceState, name="running")

			for n in serializer.validated_data['nodes']:
				tmp_req_node = {}
				tmp_req_node['profile'] = get_object_or_404(NodeProfile, pk=n['profile_id'])
				tmp_req_node['instance_size'] = get_object_or_404(InstanceSize, pk=n['instance_size_id'])
				requested_nodes.append(tmp_req_node)

			### CREATE THE DJANGO OBJECTS ###
			# todo: transaction handling? revert all if an object creation fails?
			# create the cluster
			cluster = Cluster(name=cluster_name)
			cluster.cluster_uuid = str(uuid.uuid4())
			cluster.creator = request.user
			cluster.region = subnet.vpc.region
			cluster.tm_version = tm_version
			cluster.save()

			# create the nodes and their relations to the cluster
			for requested_node in requested_nodes:
				node_uuid = str(uuid.uuid4())
				node_name = "%s_%s_%s" % (cluster.name,
											node_uuid,
											requested_node['profile'].name
											)
				tmp_node = Node(name=node_name)
				tmp_node.cluster = cluster
				tmp_node.creator = request.user
				tmp_node.tm_version = tm_version
				tmp_node.instance_size = requested_node['instance_size']
				tmp_node.subnet = subnet
				tmp_node.provisioning_status = 0
				tmp_node.node_profile = requested_node['profile']
				tmp_node.save()

				# create relations between the new node and the services specified in the node profile
				for profile_service in tmp_node.node_profile.services.all():
					tmp_service_node = ServiceNode()
					tmp_service_node.service = profile_service
					tmp_service_node.node = tmp_node
					tmp_service_node.tm_version = tm_version
					tmp_service_node.desired_state = running_state
					tmp_service_node.save()

			### RUN SALT AND STORE JOBID ###

			cluster_job = ClusterJob()
			cluster_job.cluster = cluster
			cluster_job.description = "Provisioning and Configuration for cluster: %s" % (cluster.name,)
			cluster_job.creator = request.user
			cluster_job.salt_jid = -1
			cluster_job.save()

			salt_mapfile_string = cluster.get_salt_mapfile_string()

			# todo: RUN SALT ASYNCHRONOUSLY AND OBTAIN THE JOB ID (instead of the randint placeholder)
			salt_jobid = random.randint(1000, 5000)
			# salt_jobid = salt_runner(salt_mapfile_string)

			cluster_job.salt_jid = salt_jobid
			cluster_job.save()

			return Response({
								"new_cluster_id": cluster.id,
								"salt_job_id": cluster_job.salt_jid,
								"request": serializer.validated_data
							},
							status=status.HTTP_201_CREATED
							)
		else:
			return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ClusterViewSet(ModelViewSet):
	"""
	API endpoint that allows Cluster to be viewed or edited.
	"""
	# permission_classes =
	queryset = Cluster.objects.all()
	serializer_class = ClusterSerializer

class InstanceProviderViewSet(ModelViewSet):
	"""
	API endpoint that allows InstanceProviders to be viewed or edited.
	"""
	# permission_classes =
	queryset = InstanceProvider.objects.all()
	serializer_class = InstanceProviderSerializer


class RegionViewSet(ModelViewSet):
	"""
	API endpoint that allows Regions to be viewed or edited.
	"""
	# permission_classes =
	queryset = Region.objects.all()
	serializer_class = RegionSerializer


class VPCViewSet(ModelViewSet):
	"""
	API endpoint that allows VPCs to be viewed or edited.
	"""
	# permission_classes =
	queryset = VPC.objects.all()
	serializer_class = VPCSerializer


class SubnetViewSet(ModelViewSet):
	"""
	API endpoint that allows Subnets to be viewed or edited.
	"""
	# permission_classes =
	queryset = Subnet.objects.all()
	serializer_class = SubnetSerializer


class InstanceSizeViewSet(ModelViewSet):
	"""
	API endpoint that allows Instance Sizes to be viewed or edited.
	"""
	# permission_classes =
	queryset = InstanceSize.objects.all()
	serializer_class = InstanceSizeSerializer


class NodeProfileViewSet(ModelViewSet):
	"""
	API endpoint that allows Node Profiles to be viewed or edited.
	"""
	queryset = NodeProfile.objects.all()
	serializer_class = NodeProfileSerializer


class TMVersionViewSet(ModelViewSet):
	"""
	API endpoint that allows Node Profiles to be viewed or edited.
	"""
	queryset = TMVersion.objects.all()
	serializer_class = TMVersionSerializer
