__author__ = 'dgollas'

from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required

from .views import ClusterCreationPageView, ClusterListPageView
urlpatterns = [
	#url(r'^creation$', cluster_creation),
	url(r'^creation$', login_required(ClusterCreationPageView.as_view()), name="cluster_creation_view"),
	url(r'^list$', login_required(ClusterListPageView.as_view()), name="cluster_list_view"),
]