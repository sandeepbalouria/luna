# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('provider', models.ForeignKey(to='clustermanagement.InstanceProvider')),
            ],
        ),
        migrations.CreateModel(
            name='Subnet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vanity_name', models.CharField(max_length=200)),
                ('name', models.CharField(max_length=50)),
                ('cidr', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='VPC',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=15)),
            ],
        ),
        migrations.AlterModelOptions(
            name='tmversion',
            options={'ordering': ('name',)},
        ),
        migrations.AddField(
            model_name='subnet',
            name='vpc',
            field=models.ForeignKey(to='clustermanagement.VPC'),
        ),
    ]
