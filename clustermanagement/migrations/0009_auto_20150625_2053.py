# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0008_auto_20150625_1955'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cluster',
            name='tm_version',
        ),
        migrations.RemoveField(
            model_name='node',
            name='tm_version',
        ),
    ]
