# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0011_remove_servicenode_tm_version'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicenode',
            name='tm_version',
            field=models.ForeignKey(to='clustermanagement.TMVersion', null=True),
        ),
    ]
