# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0013_remove_cluster_region'),
    ]

    operations = [
        migrations.AddField(
            model_name='cluster',
            name='region',
            field=models.ForeignKey(default=1, to='clustermanagement.Region'),
            preserve_default=False,
        ),
    ]
