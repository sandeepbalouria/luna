# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0012_servicenode_tm_version'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cluster',
            name='region',
        ),
    ]
