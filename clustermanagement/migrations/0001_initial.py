# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cluster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('description', models.TextField(max_length=500)),
                ('region', models.CharField(max_length=50)),
                ('tm_version', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='ClusterAction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('salt_jid', models.CharField(max_length=75)),
                ('description', models.TextField(max_length=500, blank=True)),
                ('target', models.CharField(default=b'', max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='ClusterActionType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='ClusterJob',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('salt_jid', models.CharField(max_length=75)),
                ('description', models.TextField(max_length=500, blank=True)),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('cluster', models.ForeignKey(to='clustermanagement.Cluster')),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ClusterLease',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('end_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('description', models.TextField(max_length=500, blank=True)),
                ('cluster', models.ForeignKey(to='clustermanagement.Cluster')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ClusterTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(max_length=500)),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-creation_date'],
            },
        ),
        migrations.CreateModel(
            name='InstanceProvider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20)),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='InstanceSize',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=b'30')),
                ('description', models.TextField(blank=True)),
                ('cpu_count', models.PositiveSmallIntegerField()),
                ('ram_amount', models.BigIntegerField(verbose_name=b'RAM (bytes)')),
                ('provider', models.ForeignKey(to='clustermanagement.InstanceProvider')),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('creation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('description', models.TextField(max_length=500)),
                ('tm_version', models.CharField(max_length=200, blank=True)),
                ('cluster', models.ForeignKey(to='clustermanagement.Cluster')),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='NodeAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=100)),
                ('value', models.TextField()),
                ('node', models.ForeignKey(to='clustermanagement.Node')),
            ],
            options={
                'ordering': ('node', 'key'),
            },
        ),
        migrations.CreateModel(
            name='NodeProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(max_length=500, blank=True)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='ServiceAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=100)),
                ('value', models.TextField()),
                ('service_node', models.ForeignKey(to='clustermanagement.Service')),
            ],
        ),
        migrations.CreateModel(
            name='ServiceNode',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('service_index', models.PositiveSmallIntegerField(default=1)),
                ('activation_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('tm_version', models.CharField(max_length=200, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ServiceNodeAttribute',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=100)),
                ('value', models.TextField()),
                ('service_node', models.ForeignKey(to='clustermanagement.ServiceNode')),
            ],
        ),
        migrations.CreateModel(
            name='ServiceState',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Tenant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(max_length=500)),
            ],
        ),
        migrations.CreateModel(
            name='TenantCluster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('move_date', models.DateTimeField(default=django.utils.timezone.now)),
                ('notes', models.TextField(max_length=b'500', blank=True)),
                ('cluster', models.ForeignKey(to='clustermanagement.Cluster')),
                ('mover', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('tenant', models.ForeignKey(to='clustermanagement.Tenant')),
            ],
        ),
        migrations.CreateModel(
            name='TMVersion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='tenant',
            name='clusters',
            field=models.ManyToManyField(to='clustermanagement.Cluster', through='clustermanagement.TenantCluster'),
        ),
        migrations.AddField(
            model_name='servicenode',
            name='desired_state',
            field=models.ForeignKey(to='clustermanagement.ServiceState'),
        ),
        migrations.AddField(
            model_name='servicenode',
            name='node',
            field=models.ForeignKey(to='clustermanagement.Node'),
        ),
        migrations.AddField(
            model_name='servicenode',
            name='service',
            field=models.ForeignKey(to='clustermanagement.Service'),
        ),
        migrations.AddField(
            model_name='node',
            name='services',
            field=models.ManyToManyField(to='clustermanagement.Service', through='clustermanagement.ServiceNode'),
        ),
        migrations.AddField(
            model_name='clusteraction',
            name='action_type',
            field=models.ForeignKey(to='clustermanagement.ClusterActionType'),
        ),
        migrations.AddField(
            model_name='clusteraction',
            name='cluster_job',
            field=models.ForeignKey(to='clustermanagement.ClusterJob'),
        ),
        migrations.AddField(
            model_name='cluster',
            name='cluster_template',
            field=models.ForeignKey(blank=True, to='clustermanagement.ClusterTemplate', null=True),
        ),
        migrations.AddField(
            model_name='cluster',
            name='creator',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
