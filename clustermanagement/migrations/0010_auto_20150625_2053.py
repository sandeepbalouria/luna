# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0009_auto_20150625_2053'),
    ]

    operations = [
        migrations.AddField(
            model_name='cluster',
            name='tm_version',
            field=models.ForeignKey(to='clustermanagement.TMVersion', null=True),
        ),
        migrations.AddField(
            model_name='node',
            name='tm_version',
            field=models.ForeignKey(to='clustermanagement.TMVersion', null=True),
        ),
    ]
