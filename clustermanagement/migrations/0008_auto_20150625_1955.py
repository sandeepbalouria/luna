# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0007_auto_20150625_1925'),
    ]

    operations = [
        migrations.AddField(
            model_name='node',
            name='node_profile',
            field=models.ForeignKey(to='clustermanagement.NodeProfile', null=True),
        ),
        migrations.AddField(
            model_name='node',
            name='provisioning_status',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='node',
            name='subnet',
            field=models.ForeignKey(to='clustermanagement.Subnet', null=True),
        ),
        migrations.AlterField(
            model_name='nodeprofile',
            name='services',
            field=models.ManyToManyField(to='clustermanagement.Service'),
        ),
    ]
