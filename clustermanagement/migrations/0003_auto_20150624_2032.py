# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0002_auto_20150624_0004'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServiceProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('service', models.ForeignKey(to='clustermanagement.Service')),
            ],
        ),
        migrations.AddField(
            model_name='vpc',
            name='region',
            field=models.ForeignKey(to='clustermanagement.Region', null=True),
        ),
        migrations.AddField(
            model_name='nodeprofile',
            name='services',
            field=models.ManyToManyField(to='clustermanagement.ServiceProfile'),
        ),
    ]
