# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0006_nodeprofile_map_template'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='nodeprofile',
            name='map_template',
        ),
        migrations.AddField(
            model_name='nodeprofile',
            name='map_template_name',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='nodeprofile',
            name='salt_instance_profile_name',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
    ]
