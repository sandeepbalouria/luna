# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0003_auto_20150624_2032'),
    ]

    operations = [
        migrations.AddField(
            model_name='node',
            name='instance_size',
            field=models.ForeignKey(default=1, to='clustermanagement.InstanceSize'),
            preserve_default=False,
        ),
    ]
