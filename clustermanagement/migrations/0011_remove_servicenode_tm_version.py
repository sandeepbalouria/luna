# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0010_auto_20150625_2053'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='servicenode',
            name='tm_version',
        ),
    ]
