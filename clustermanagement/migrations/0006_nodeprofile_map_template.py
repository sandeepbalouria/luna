# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0005_cluster_cluster_uuid'),
    ]

    operations = [
        migrations.AddField(
            model_name='nodeprofile',
            name='map_template',
            field=models.TextField(blank=True),
        ),
    ]
