# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('clustermanagement', '0004_node_instance_size'),
    ]

    operations = [
        migrations.AddField(
            model_name='cluster',
            name='cluster_uuid',
            field=models.UUIDField(null=True, blank=True),
        ),
    ]
