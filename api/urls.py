from django.conf.urls import url, include
from rest_framework import routers
from account.views import UserViewSet, GroupViewSet
from clustermanagement.views import InstanceSizeViewSet, ClusterViewSet ,NodeProfileViewSet, TMVersionViewSet, ClusterCreationView, \
	SubnetViewSet, VPCViewSet, RegionViewSet, InstanceProviderViewSet

router = routers.DefaultRouter()
router.register(r'auth/users', viewset=UserViewSet)
router.register(r'auth/groups', viewset=GroupViewSet)
router.register(r'clustermanagement/clusters', viewset=ClusterViewSet)
router.register(r'clustermanagement/instancesizes', viewset=InstanceSizeViewSet)
router.register(r'clustermanagement/nodeprofiles', viewset=NodeProfileViewSet)
router.register(r'clustermanagement/tmversions', viewset=TMVersionViewSet)
router.register(r'clustermanagement/instanceproviders', viewset=InstanceProviderViewSet)
router.register(r'clustermanagement/vpcs', viewset=VPCViewSet)
router.register(r'clustermanagement/regions', viewset=RegionViewSet)
router.register(r'clustermanagement/subnets', viewset=SubnetViewSet)


urlpatterns = [
	url(r'^', include(router.urls)),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

	url(r'^clustermanagement/cluster/createcluster/', ClusterCreationView.as_view())
]