#!/bin/sh
#This script will create a standarized virtual environment for someone who just pulled the project.
PROJECT_NAME="luna"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd -P "$( dirname "$SOURCE" )" && pwd )"
ENV_NAME=$PROJECT_NAME"_venv"
PYTHON_REQUIREMENTS_FILE=$DIR"/python_requirements.txt"
echo $DIR
cd $DIR/..
echo "Creating virtual env "$ENV_NAME" in "`pwd`
virtualenv $ENV_NAME
ACTIVATION_SRC=$ENV_NAME"/bin/activate"
echo "Activating "$ACTIVATION_SRC
source $ACTIVATION_SRC
echo "Installing python requirements from "$PYTHON_REQUIREMENTS_FILE
pip install -r $PYTHON_REQUIREMENTS_FILE
echo ""
cd -
echo "Done bootstrapping the environment. The virtual env is currently active. To use it later without running this script just run source "$ACTIVATION_SRC
