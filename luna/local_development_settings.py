#this file overrides whatever settings are in settings.py when the LUNA_DEVELOPMENT env variable is set to 1.

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'luna',
        'USER': 'luna_user',
        'PASSWORD': 'luna_user',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}