
var clusterManagementApp = angular.module('clusterManagementApp', []);

clusterManagementApp.config(function($interpolateProvider, $httpProvider){
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

clusterManagementApp.filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);

clusterManagementApp.controller('ClusterCreationCtrl', function($scope, $http){


    var NODE_PROFILES_URL = "/api/clustermanagement/nodeprofiles/?format=json";
    var INSTANCE_SIZES_URL = "/api/clustermanagement/instancesizes/?format=json";
    var TM_VERSIONS_URL = "/api/clustermanagement/tmversions/?format=json";
    var SUBNETS_URL = "/api/clustermanagement/subnets/?format=json";
    var CLUSTER_CREATION_URL = "/api/clustermanagement/cluster/createcluster/";

    //get the available node_profiles from the server
    $http.get(NODE_PROFILES_URL).success(function(data) {
        $scope.node_profiles = data.results;

    });

    //get the available subnets from the server
    $http.get(SUBNETS_URL).success(function(data) {
        $scope.subnets = data.results;
        if($scope.subnets.length > 0){
            $scope.subnet = $scope.subnets[0];
    }
    });

    //get the available instance sizes from the server
    $http.get(INSTANCE_SIZES_URL).success(function(data) {
        $scope.instance_sizes = data.results;
    });

    //get the available tm_versions from the server
    $http.get(TM_VERSIONS_URL).success(function(data) {
        $scope.tm_versions = data.results;
        if($scope.tm_versions.length > 0){
            $scope.tm_version = $scope.tm_versions[0];
        }
    });

    //temporary storage for the nodes we will create
    $scope.new_nodes = [

    ];

    //helper function to find an element inside an array based on it's id property.
    function find_by_id(theArray, theId){
        return theArray.find(function(obj){
            return obj.id == theId;
        })
    }

    $scope.addInstance= function(instance_size, instance_profile ){
        if(instance_size && instance_profile){

            var the_node_profile = find_by_id($scope.node_profiles, parseInt(instance_profile));
            var the_instance_size = find_by_id($scope.instance_sizes, parseInt(instance_size));

            $scope.new_nodes.push({node_profile:the_node_profile, instance_size: the_instance_size});
        }
    };

    $scope.removeInstance= function(pIndex){
        $scope.new_nodes.splice(pIndex, 1);
    };

    $scope.submit_disabled = false;
    $scope.input_errors = {};
    $scope.cluster_creation_results = [];

    $scope.createCluster= function() {
        if ($scope.submit_disabled == false){

            var theNodes = [];

            $scope.new_nodes.forEach(function(element, index, array){
               theNodes.push({profile_id:element.node_profile.id, instance_size_id:element.instance_size.id})
            });

            var cluster_json = {
                name: $scope.cluster_name,
                tm_version_id: $scope.tm_version,
                subnet_id: $scope.subnet,
                nodes: theNodes
            };
            $scope.submit_disabled = true;
            $http.post(
                CLUSTER_CREATION_URL, cluster_json
            ).success(function (data, status, headers) {
                    console.log("Success!: " + JSON.stringify(data));
                    $scope.submit_disabled = false;
                    $scope.input_errors = {};
                    var stringified_data = JSON.stringify(data, undefined, 4);
                    var stringified_headers = JSON.stringify(headers(), undefined, 4);
                    $scope.cluster_creation_results.push(
                        {
                            date : new Date(),
                            status: status,
                            html_headers: syntaxHighlight(stringified_headers),
                            data: data,
                            html_data : syntaxHighlight(stringified_data)
                        }
                    )
                }
            ).error(function (data, status, headers) {
                    console.log("Error!: " + JSON.stringify(data));
                    $scope.input_errors = data;
                    $scope.submit_disabled = false;
                    var stringified_data = JSON.stringify(data, undefined, 4)
                    var stringified_headers = JSON.stringify(headers(), undefined, 4)
                    $scope.cluster_creation_results.push(
                        {
                            date : new Date(),
                            status: status,
                            html_headers: syntaxHighlight(stringified_headers),
                            data: data,
                            html_data : syntaxHighlight(stringified_data)
                        }
                    )
                }
            )
        }
    };

});




