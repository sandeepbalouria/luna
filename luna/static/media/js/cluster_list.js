
var clusterListApp = angular.module('clusterListApp', []);

clusterListApp.config(function($interpolateProvider, $httpProvider){
    $interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

clusterListApp.filter('to_trusted', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
    }]);

clusterListApp.controller('ClusterListCtrl', function($scope, $http){


    var CLUSTER_LIST_URL = "/api/clustermanagement/clusters/?format=json";


    //get the available clusters from the server
    $http.get(CLUSTER_LIST_URL).success(function(data) {
        $scope.cluster_list = data.results;
    });

    //helper function to find an element inside an array based on it's id property.
    function find_by_id(theArray, theId){
        return theArray.find(function(obj){
            return obj.id == theId;
        })
    }

    $scope.selectCluster= function(cluster_id){

        //todo: load the details for the cluster
        $scope.loading_details = true;
        $scope.current_cluster = find_by_id($scope.cluster_list, cluster_id);
        var CLUSTER_DETAILS_URL = "/api/clustermanagement/clusters/"+String(cluster_id)+"?format=json";
        $http.get(CLUSTER_DETAILS_URL).success(function(data) {
            $scope.current_cluster = data;
        });

    };

});




