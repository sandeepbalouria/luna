from django.shortcuts import render_to_response
from django.template import RequestContext


from django.views.generic import TemplateView


class HomeView(TemplateView):
	template_name = "luna/home.html"

	def get_context_data(self, **kwargs):
		context = {}
		return context