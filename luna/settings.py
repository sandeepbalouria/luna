"""
Django settings for luna project.

Generated by 'django-admin startproject' using Django 1.8.2.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'g6gdsvltc(e8++msteh+p0+w6+^w8^mzk7@7n7o=oe8)(+il_2'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
	'rest_framework',
	'clustermanagement'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'luna.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'luna.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'realusername',
        'USER': 'arealpassword',
        'PASSWORD': 'luna_user',
        'HOST': 'areal_rds_instance_or_something',
        'PORT': '5432',
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

####### EMAIL SETTINGS ###########
EMAIL_BACKEND = 'django_ses.SESBackend'
DEFAULT_FROM_EMAIL = 'mailer@tidemark.com'
AWS_SES_REGION_NAME = "us-west-2"
AWS_SES_REGION_ENDPOINT = 'email.us-west-2.amazonaws.com'
SERVER_EMAIL = 'mailer@tidemark.com'

#restricted IAM that can only send emails (luna_ses_user is authorized to SendEmail, SendRawEmail, GetSendQuota).
AWS_SES_ACCESS_KEY_ID = 'AKIAIMFGCLO2N42EYL5Q'
AWS_SES_SECRET_ACCESS_KEY = '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'
#################################

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOGIN_REDIRECT_URL = "/"

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/


# URL prefix for static files.
STATIC_URL = '/static/'
# Example: "http://media.lawrence.com/static/"
# # Additional locations of static files
STATICFILES_DIRS = (
	os.path.join(BASE_DIR, "luna/static/"),
)
#STATIC_ROOT = os.path.join(BASE_DIR, "luna/static/")

#
# # List of finder classes that know how to find static files in
# # various locations.
# STATICFILES_FINDERS = (
#     'django.contrib.staticfiles.finders.FileSystemFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder'
# )



REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': ('rest_framework.permissions.IsAdminUser',),
    'PAGE_SIZE': 10
}

#if the LUNA_DEVELOPMENT env variable is set to 1, we override some settings
luna_env = os.environ.get("LUNA_ENV","PROD")
if luna_env == "DEV":
	from development_settings import *
elif luna_env == "LOCAL_DEV":
    from local_development_settings import *