#this file overrides whatever settings are in settings.py when the LUNA_DEVELOPMENT env variable is set to 1.

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'luna',
        'USER': 'luna_user',
        'PASSWORD': 'luna_user',
        'HOST': 'some_host_on_a_dev_server',
        'PORT': '5432',
    }
}