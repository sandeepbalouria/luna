from django.shortcuts import render, render_to_response
from django.contrib.auth.decorators import login_required
from django.template import RequestContext

# Create your views here.

@login_required
def log_viewer(request):
	context = {}
	return render_to_response( "log_viewer/log_viewer.html", context=context, context_instance=RequestContext(request))


