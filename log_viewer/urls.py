from django.conf.urls import include, url
from django.contrib import admin

from log_viewer.views import log_viewer

urlpatterns = [

	url(r'^$', log_viewer),

]