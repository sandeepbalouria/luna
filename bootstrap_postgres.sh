#!/bin/sh
#This script will create the base database for luna development, as well as the development user.
#It assumes you have installed a version of postgres and that it is running on port 5432 with master user postgres.
PROJECT_NAME="luna"
USERNAME=$PROJECT_NAME"_user"
DB_NAME=$PROJECT_NAME
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && cd -P "$( dirname "$SOURCE" )" && pwd )"
DB_CMD="CREATE DATABASE "$PROJECT_NAME"; CREATE USER "$USERNAME" WITH PASSWORD '"$USERNAME"'; GRANT ALL PRIVILEGES ON DATABASE "$DB_NAME" TO "$USERNAME";"
echo "Executing this on psql: "$DB_CMD
echo $DB_CMD | psql -hlocalhost -Upostgres
echo ""
echo "Done bootstrapping the database, to initialize the "$PROJECT_NAME" Django application tables please run python manage.py migrate"

